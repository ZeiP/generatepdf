import cStringIO as StringIO
import ho.pisa as pisa
from sx.pisa3.pisa_pdf import *
from django.template.loader import get_template
from django.template import Context
from django.http import HttpResponse
from cgi import escape
# PDF gen
from weasyprint import HTML

from PIL import Image

def render_to_pdf(template_src, context_dict, additional=[]):
  pdf = save_pdf(template_src, context_dict, additional)
  return HttpResponse(pdf.getvalue(), content_type='application/pdf')

def save_pdf(template_src, context_dict, additional=[]):
  template = get_template(template_src)
  context = Context(context_dict)
  html  = template.render(context)
  viewhtml = HTML(string=html)
#  result = StringIO.StringIO()

  pdf = pisaPDF()
#  subPdf = pisa.pisaDocument(StringIO.StringIO(html.encode("UTF-8")), result)
  subPdf = viewhtml.write_pdf()
  pdf.addFromString(subPdf)

  return pdf
