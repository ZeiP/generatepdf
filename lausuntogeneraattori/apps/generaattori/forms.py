# -*- coding: utf-8 -*-

from django import forms
from generaattori.models import StatementItem

CHOICES = [
  ('Y', 'Kannatan'),
  ('N', 'En kannata'),
  ('E', 'Ei kantaa'),
]

class SivStatementForm(forms.Form):
  organisation = forms.CharField(max_length=255, required=False)
  name = forms.CharField(max_length=255)
  occupation = forms.CharField(max_length=255, required=False)
  address = forms.CharField(max_length=255)
  city = forms.CharField(max_length=255)

  def __init__(self, *args, **kwargs):
    super(SivStatementForm, self).__init__(*args, **kwargs)
    selects = {}
    notes = {}
    for item in StatementItem.objects.all():
      id = item.id
      select_name = 'select_' + str(id)
      notes_name = 'notes_' + str(id)
      self.fields[select_name] = forms.ChoiceField(label='', choices=CHOICES, widget=forms.RadioSelect, required=False)
      self.fields[notes_name] = forms.CharField(label='Perustelut', widget=forms.Textarea, required=False)
