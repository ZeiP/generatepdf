# -*- coding: utf-8 -*-

from generaattori.helpers import render_to_pdf
from generaattori.forms import SivStatementForm
from generaattori.models import StatementItem
from django.shortcuts import render

from generaattori.forms import CHOICES

def generate(request):
  items = StatementItem.objects.all()
  if request.method == 'POST':
    form = SivStatementForm(request.POST)
    if form.is_valid():
      for item in items:
        if form.cleaned_data['select_' + str(item.id)]:
          item.select_value = dict(CHOICES)[form.cleaned_data['select_' + str(item.id)]]
        item.notes_value = form.cleaned_data['notes_' + str(item.id)]
      return render_to_pdf('lausunto.html', {
        'data': form.cleaned_data,
        'items': items,
      })
  else:
    form = SivStatementForm()
    for item in items:
      item.select_field = form['select_' + str(item.id)]
      item.notes_field = form['notes_' + str(item.id)]
    return render(request, 'form.html', {
      'form': form,
      'items': items,
    })
