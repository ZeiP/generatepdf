from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'lausuntogeneraattori.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^generate', 'generaattori.views.generate', name='generate'),
)
#)  + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
