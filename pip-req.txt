django

# PDF generation
reportlab==2.6
pisa
html5lib
pypdf
#PIL
pil

# Better PDF generation
WeasyPrint

south
