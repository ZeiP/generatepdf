lausuntogeneraattori v. 0.0.1

Author: Jyri-Petteri Paloposki <jyri-petteri.paloposki@iki.fi>
License: see LICENSE file.

Description:
------------



Installing:
-----------

As usual with a Django project:

# Checkout the project to a random directory (and change to the directory)

# Make a new virtualenv
$ virtualenv env

# Change to the virtualenv
$ source env/bin/activate

# Install required libraries etc.
$ pip install -r pip-req.txt

Run it, using manage.py runserver or whatever you like.